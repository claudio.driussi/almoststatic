API for Almoststatic
====================

Almoststatic is composed by two classes, Almoststatic is the base class that
do all the job and FlaskAlmoststatic that simply bind Almoststatic to a Flask
app.

-----

.. autoclass:: almoststatic.Almoststatic
   :members: load_config, build_page, write_static, write_json_meta, invalidate_cache

-----

.. autoclass:: almoststatic.FlaskAlmoststatic
   :members: init_app

