.. Almoststatic documentation master file, created by
   sphinx-quickstart on Mon Jul 12 15:58:10 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: /_static/Almoststatic.png
    :alt: Almoststatic
    :align: center
    :target: https://gitlab.com/claudio.driussi/almoststatic

Welcome to Almoststatic's documentation!
========================================

Almoststatic is a static sites and web pages generator engine written in
`Python <https://python.org/>`_ which utilizes the
`Jinja2 template system <https://jinja.palletsprojects.com/>`_ to render pages. It can be
integrated with `Flask <https://flask.palletsprojects.com/>`_ apps to serve
static contents on dynamic sites or used standalone for static
sites development.

Pages are declared in `YAML <https://yaml.org/>`_ files and rendered with Jinja2
template files. The HTML contents can be written in `Markdown <https://www.markdownguide.org/>`_
markup language or in plain HTML. The "content" folder contains all data needed to
do the job, and the "config.yaml" is used to share global parameters and to set up
configuration.

It is loosely inspired by `Hugo <https://gohugo.io/>`_ static site generator,
but it differs in many ways.


Why Almoststatic?
-----------------

There are many static site generators such as `Next.js <https://nextjs.org/>`_,
`Hugo <https://gohugo.io/>`_ or `Jekyll <https://jekyllrb.com/>`_, but you
might prefer Almostatic because:

- **It's perfect for pythonist.** It uses Jinja2 and Flask, widely used by the
  Python community, eliminating the need to learn other programming languages or
  template engines.

- **It's easy to use** The rules are minimal, requiring you to learn only a few
  things, offering great flexibility.

- **It's versatile.** The engine has a powerful recursive system for embedding
  and including content. This helps in building rich content and splitting it
  into small, maintainable pieces called "widgets" (see :doc:`pages`).

- **It has blog capabilities.** Contains functions to query metadata info useful
  for organizing blog pages by categories and tags.

- **You can deliver static and dynamic contents at same time.** With Flask you
  can build your dynamic content and let Almoststatic to render the rest of page
  or the entire page, if it is full static.

- **Write static sites.** Static sites composed only of text files and media
  contents are easy to deliver on the web, secure by design, require less
  maintenance and resources, and are faster. If you have no need for dynamic
  content, **Almoststatic** allows you to write all pages as static.

- **Not only for pythonists.** Basic knowledge of python is needed, but once
  your developer environment is ready, Almoststatic lets you focus on writing
  YAML and Markdown content and creating your own widgets in HTML and Jinja2.

Project status
--------------

Almoststatic is young but stable! It has all the planned features and has
consistently delivered the right results, making it "production-ready."

At the moment there are only a few themes. we are developing a "rolling theme"
with some beautiful widgets ready to use.
See `Flatstep theme <https://gitlab.com/almoststatic-themes/flatstep/>`_ and
follow the tutorial.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart.rst
   structure.rst
   pages.rst
   embedding.rst
   processors.rst
   templates.rst
   static.rst
   api.rst
   changelog.rst

