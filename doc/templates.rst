Templates
=========

Templates in Almoststatic are written in `Jinja2 <https://jinja.palletsprojects.com/>`_.

Here are some key rules and conventions added by Almoststatic:

- In each page, you can set the ``templ_name`` metadata field. If not set, it defaults to ``page.html``.
  Here's an example:

  ::

      {%extends page.extends or "base.html" %}
      {%block body%}
      <main role="main">
      {{content}}
      </main>
      {%endblock%}

  The ``content`` field holds the list of widgets for the page.

- The ``extends`` metadata field in page can change the *base* template.
  If not present, it defaults to a base template. Here's a simplified example:

  ::

     <!DOCTYPE html>
     <head>
     <title>{{page.title}}</title>
     <meta name="description" content="{{page.description}}">
     <meta name="author" content="{{page.author}}">
     <meta name="generator" content="{{meta.generator}}">
     <link rel="stylesheet" href="{{get_media('css/main.css')}}">
     </head>
     <body>
     <div class="container">
        {% block body %}
        {% endblock %}
     </div>
     <script src="{{get_media('js/main.js')}}" type="text/javascript"></script>
     </body>
     </html>

- The ``content`` part of a YAML page file, is al list of ``widgets``, each
  widget must have his template file, For instance, the ``tabbed`` widget must have a
  ``tabbed.html`` like this:

  ::

    {%include "include/widget_style.html" -%}
    <div id="{{widget.id}}" class="{{widget.class}}">
    <ul class="nav {{widget.tabs_style or 'nav-tabs'}}" role="tablist">
        {% for i,item in enum_list(widget.tabs) -%}
        <li class="nav-item" role="presentation">
        <button class="nav-link {{' active' if item.active else ''}}" id="nav-{{widget.id}}{{i}}-tab" data-bs-toggle="tab"
            data-bs-target="#nav-{{widget.id}}{{i}}" type="button" role="tab" aria-controls="{{item.label}}"
            aria-selected="{{'true' if item.active else 'false'}}">{{item.label}}</button>
        </li>
        {% endfor -%}
    </ul>
    <div class="tab-content" id="nav-{{widget.id}}-content">
        {% for i,item in enum_list(widget.tabs) -%}
        <div class="tab-pane fade{{' show active' if item.active else ''}}" id="nav-{{widget.id}}{{i}}" role="tabpanel"
        aria-labelledby="nav-{{widget.id}}{{i}}-tab">{{item.text|safe}}</div>
        {% endfor -%}
    </div>
    </div>

  Within a widget template you have access to the ``widget`` object with all values
  declared into yaml file.

- In each template, you have access to the ``page`` object with all its metadata
  fields, the content list, and any extra fields added to it.

- In each template you have access to global data declared in ``config.yaml``
  file also as a dictionary in the ``config`` field, for example, if you have
  into config file a list called ``MainMenu``, you can write something like:

  ::

    {% for item in config[page.menu] or MainMenu -%}
    <li>
    <a href="{{get_url(item.url)}}">{{ item.name }}</a>
    </li>
    {% endfor -%}
