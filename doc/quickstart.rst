Quick start
===========

The simplest way to determine if **Almoststatic** is what you're looking for is
to try the sample provided with the source code package and explore the source code.

The following tutorial is tested on Linux Ubuntu, but it's easy to port to
other platforms such as different Linux distros, Windows, or Mac.

You need git and Python 3; install them with:

::

   $ sudo apt install git python3
   $ sudo apt install python3-dev
   $ sudo apt install python3-pip
   $ sudo apt install python3-venv

The latest Linux releases come with Python 3 already installed. Check if your
version is 3.6 or higher:

::

   $ python3 --version

Now clone Almoststatic with git and enter into the directory:

::

   $ git clone https://gitlab.com/claudio.driussi/almoststatic.git
   $ cd almoststatic

It's common in Python to use ``virtualenv`` for development. To do so and to install
Almoststatic, write:

::

   $ python3 -m venv myvenv
   $ source myvenv/bin/activate
   $ pip install almoststatic
   $ pip install pyftpsync

Done! Now you can try the sample, ``cd`` into sample directory and run flaskapp.py:

::

   $ cd sample/
   $ python flaskapp.py

A local Flask App instance server is executed, and you can test Almoststatic
features.

Open your browser and copy-paste the following URL: ``http://127.0.0.1:5000/``
or ``localhost:5000``

You will be redirected to the static site showing some widgets and features of
Almostatic. You can navigate to see some examples.

You can always exit from the server by pressing CTRL-C on your terminal.

To build the static site, navigate to the ``/write_static`` page and click the
"Do it" button, or run:

::

   $ python write_static.py
   $ ls -l ../_static_site/

Several ``*.html`` files are created in the ``../_static_site/`` folder, but to get
a really working static site, see the configuration at: :doc:`static`

If you wish, you can do:

::

   $ cd ../test
   $ python as_test.py

to run automatic testing.

I suggest running it in the debugger to examine how Almoststatic works in simpler cases.

