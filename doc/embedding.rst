Embedding and including
=======================

Embedding
---------

An Almoststatic page is composed of a list of widgets arranged vertically,
with each subsequent widget appearing below the previous one.

However, this structure may not always suffice. There are instances where there
is a need to embed a widget within another. For example, in a tabbed or
accordion content, you may need to include an image row or some other widget.
Additionally, you might want to arrange contents in columns, necessitating the
embedding of contents in each column.

Almoststatic utilizes a recursive algorithm that makes it easy to achieve such
embedding. For instance, you can embed an accordion within a tabbed:

::

  - type: tabbed
    id: my_tab
    tabs:
      - label: tab1
        text: |
          ## tab 1
          Simple text

      - label: tab2
        text:
          content:
            - type: accordion
              id: fisa1
              cards:
                - button: "Card 1"
                  text: |
                    Text1
                - button: "Card 2"
                  text:
                    content:
                      - type: image_row
                        image: landscape1.jpg
                        text: |
                          my lorem content
                - button: "Card 3"
                  text: |
                    Text3

As illustrated, you can have an accordion within a tabbed, and an image_row
within the accordion.

Moreover, embedding can be nested at each level, with the nesting limit being
determined by the desired result quality.

To achieve this, simply place a ``content`` field list into the ``text`` field.

Almoststatic offers two other powerful methods for embedding contents into the
``text``. By using the ``embed`` and ``particle`` commands, you can insert
widgets wherever needed.

::

  content:
    - type: text_html
      text: "{{embed(page.txt1)}}"

    - type: text_html
      text: "{{particle('txt2')}}"

  txt1:
    - type: text_html
      text: My embedded content

  particles:
    - type: text_html
      id: txt2
      text: My particle

The choice between particle and embed is a matter of preference. ``particle`` is
clean and tidy, while ``embed`` can be more powerful.

Including
---------

Everything achievable with embedding can also be done with including.

Nested embedding on long pages might be challenging to maintain and keep organized.
For instance, there could be long text that is convenient to keep separated or
contents that should be present in multiple pages.

For these reasons, pages can be split into page and inclusion files:

::

  - type: tabbed
    id: my_tab
    tabs:
      - label: tab1
        text:
          include: include/page1/mytext.md

      - label: tab2
        text:
          include: include/page1/accordion.yaml

      - label: tab3
        text:
          include: include/page1/mytext.html

  - type: include
    file: include/page1/rest_of_page.yaml

In this example, the page is split into a page and inclusions. The keyword
``include`` is placed within the ``text`` keyword. Additionally, a special
widget called ``include`` can be used outside other widgets.

Inclusion files can be stored anywhere within the ``content`` folder except the
``pages`` folder, which is reserved for URL pages.

Almoststatic recognizes three types of inclusion file extensions: ``*.html``
for plain HTML files, ``*.md`` for Markdown files, and ``*.yaml`` for files
with a list of widgets.

As with embeddings, included files can have nested inclusions and embeddings.
The only limit is the quality of the result.
