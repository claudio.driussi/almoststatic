Context processors
==================

Almoststatic introduces several commands to Jinja2 through a mechanism known as
``context processors``, (in terms of Jinja and Flask). These commands can be
utilized in template files and within YAML contents.

The available commands are:

enum_list
---------

This command adds the Python ``enumerate`` function to templates, useful for
obtaining a unique ID for each item in a list within a template.

::

    {% for i,item in enum_list(widget.tabs) -%}
    <li id="{{widget.id}}{{i}}">{{ item.text|safe }}
    </li>
    {% endfor -%}

len_list
--------

This command adds the Python ``len`` function to templates, allowing to get the
length of a list or a dictionary.

get_markdown
------------

Transform a Markdown text into html, usually you don't need to call it because
is automatic called at each widget.text rendering.

::

    {% set myvar = "# this is a title" %}
    {{ get_markdown(myvar) }}


get_media
---------

``get_media`` is a very important command.

Flask by default search media files into folder ``./static`` using the
command ``{{ url_for('static', filename='...') }}``.

Almoststatic, to encapsulate all contents within the ``content`` folder replace
the ``url_for`` command with ``get_media`` and this helps to configure media
destination folder when writing static pages.

replace:

::

   <link rel="stylesheet" href="{{url_for('static', filename='css/main.css')}}">

with:

::

   <link rel="stylesheet" href="{{get_media('css/main.css')}}">

And put files into ``content/media`` directory.

But if you really need to use ``url_for`` command, you can write a special
Flask app dedicated to write static files, see samples.

But there is more. ``get_media`` is recognized even into contents on yaml
files:

::

  - type: jumbotron
    id: jumbo1
    style: '
      background-image: url({{get_media("gradient1.jpg")}});
      background-size: cover contain;
      min-width: 100%;
      height: 16em;
      color: white;
      '

in this case we add some style to the widget and within the css we call an
image from media folder.

get_url
-------

Internal url's of our site, can change from Flask app to static site, for this
reason we have a function that build the url according to environment. The url
is composed by a prefix, the url that is the same of path and name of pages and
a suffix.

Internal url's paths by default are all relatives, so if you write a static
site, it can be navigated also from filesystem. For this reason prefix usually
is an empty string, but if you need, you can force a prefix for the static site.

So:

::

   <a href="{{get_url('index')}}">Index</a>

return ``index`` on flask and can return something like ``/mysite/index.html``
on static site.

Note that url's passed to ``get_url`` don't have leading or trailing dashes.

.. note::
   There is an incompatibility between markdown links and Jinja2 fields, so we
   can't use single or double quote within links in markdown texts,
   the shortcut ``[Index]({{get_url('index')}})`` is translated in a wrong way.
   For this reason we added to Almoststatic the variable ``quote_escape`` to
   replace quotes at runtime, and we can write ``[Index]({{get_url(^^index^^)}})``
   to get the right result. The sequence ``^^`` usually is not used into texts,
   but if you need it, you can change the default to a sequence of your choice.


embed
-----

Do the same thing that can be done with :doc:`embedding`. Can be useful to embed
contents for not recognized areas of page (see :doc:`pages`):

::

        <aside>
          {% if page.aside %}
          {{embed(page.aside)|safe}}
          {% else %}
          {{include('include/aside.yaml')|safe}}
          {% endif %}
        </aside>

In this case if page contain a list called ``aside``, the content is rendered
in the ``<aside></aside>`` section of page.

particle
--------

Similar to ``embed`` recognizes widgets within a section of a page called
by default ``particles`` and include them into text searching the ``id``.

See :doc:`embedding`.

include
-------

Like ``embed`` let embedding contents at the template level, ``include`` let
including files at template level (and even within page texts.)

As you can see in the previous sample, the ``page.aside`` list is included
in page, but if it is not present, the file ``include/aside.yaml`` is included.

query_blog
----------

This is the heart of blog capabilities of Almoststatic. It return three values.
a list of found pages, the list of pages belonging to the given categories and
the list of pages with given tags.

::

    {%- set query = query_blog(categories=page.blog.categories, tags=page.blog.tags) -%}
    {%- set pages = query[2] -%}
    {%- if len_list(pages) -%}
      {%-for k in pages -%}
        {{ k }} ({{ len_list(pages[k]) }}) <br>
        {%- for i in pages[k] %}
          <a href="{{ get_url(i.pagename) }}">{{ i.title }}</a>
        {%- endfor -%}
        <br>
      {%- endfor -%}
    {%- endif -%}

In this case we list all pages with the same tags of the current page

pages_id
--------

This is an utility function that can be used with some javascript on the frontend.
Almoststatic always save a file called ``site_meta.json`` on the root of ``media``
folder.

It contain all metadata info for all pages, each page contain a unique ID, so with:

::

    {%- set query = query_blog(categories=page.blog.categories, tags=page.blog.tags) -%}
    {%- set pages = pages_id(query[0]) -%}

You get the list of pages to pass to your script.

Discontinued and deprecated
---------------------------

The following processors can be easily be substituted with standard Jinja2
commands, so they can be considered deprecated.

iif
...

Inline if a shortcode for if/else into templates. Syntax is
``iif(condition, if_true, if_false="")``.

Instead to write:

::

    {{'active' if page.pagename == item.url else ""}}
    {# or #}
    {{'active' if page.pagename == item.url else "not-active"}}

Simply write:

::

    {{iif(page.pagename == item.url,'active')}}
    {# or #}
    {{iif(page.pagename == item.url,'active','not-active')}}


if_in
.....

Shortcode to check for list inclusion. Syntax is
``if_in(value, iterable, if_not="")``.

Try:

::

    {{if_in('carousel-caption-bg', widget.tags)}}
    {# or #}
    {{if_in('carousel-caption-bg', widget.tags, 'my-class')}}

This is useful to add classes on arbitrary positions into widgets.
