"""
"""
import unittest
import os

import almoststatic

JSITE = './content/media/site_meta.json'


class TestAS(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        try:
            os.remove(JSITE)
        except Exception:
            pass
        cls.a = almoststatic.Almoststatic()
        cls.a.load_config()
        cls.a.build_page('home')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def setUp(self):
        self.a.invalidate_cache()

    def test_as(self):
        """Basic test"""
        s = self.a.build_page('home')
        # print(s)
        # values declared in config.yaml are set global
        self.assertIn("This: almoststatic", s)
        # enum_list function is added to Jinja2
        self.assertIn("0 zero", s)
        # variables coming from page
        self.assertIn("<title>Home page</title>", s)
        # at first build_page the metadata file is written
        self.assertTrue(os.path.exists(JSITE))

    def test_sub(self):
        """Page in subfolder"""
        s = self.a.build_page('page1/page1')
        # print(s)
        self.assertIn("This: almoststatic", s)
        self.assertIn("<title>Subfolder page</title>", s)

    def test_page1(self):
        """Page with same name of sub folder, load different config file.
        automatic markdown conversion, dynamic content
        """
        self.a.load_config("./content/page1_cfg.yaml")
        s = self.a.build_page('page1', dynamic="Some dynamic content")
        # print(s)
        self.assertIn("This: new_config", s)
        self.assertIn(
            "No conflict with subfolder and pages with same name", s)
        self.assertIn("Some dynamic content", s)

        # restore default config
        self.a.load_config()

    def test_page2(self):
        """Multiple widgets, composed widgets"""
        s = self.a.build_page('page2')
        # print(s)
        self.assertIn("first content row", s)
        self.assertIn("second content row", s)
        self.assertIn("tab 2", s)
        self.assertIn("content 3", s)
        self.assertIn("<li>peaches</li>", s)

    def test_extends(self):
        """templ_name and extends page directives"""
        s = self.a.build_page('blog/blog1')
        # print(s)
        self.assertIn("this is a blog base page sample", s)
        self.assertIn("Blog page style", s)

    def test_embedding(self):
        """Embedding recursive widgets"""
        s = self.a.build_page('embedding')
        # with open("embedding_test.html", "w") as f:
        #     f.write(s)
        #     f.close()
        # print(s)
        self.assertIn("embedded list 1", s)
        self.assertIn("nested tab 1", s)
        self.assertIn("recursive content", s)

    def test_including(self):
        """including widgets"""
        s = self.a.build_page('including')
        # print(s)
        self.assertIn("<th>Title</th>", s)
        self.assertIn("from file include/simple/include.html", s)
        self.assertIn("from file include/simple/include.md", s)
        self.assertIn("tab 2 with including and embedding", s)
        self.assertIn("included from include1.yaml", s)

    def test_render(self):
        """including widgets"""
        s = self.a.build_page('render')
        # print(s)
        self.assertIn("lorem embedded jinja2 tags ipsum", s)

    def test_cached(self):
        """Check cache optimization"""

        # this page is not cacheable so was_cached is always false
        s = self.a.build_page('page1', dynamic="Some dynamic content")
        s = self.a.build_page('page1', dynamic="Some dynamic content")
        self.assertFalse(self.a.was_cached)

        # read some pages
        s = self.a.build_page('home')
        self.assertFalse(self.a.was_cached)
        s = self.a.build_page('render')

        # page "home" should be cached
        s = self.a.build_page('home')
        self.assertTrue(self.a.was_cached)
        # pages "home" and "render" are cached
        self.assertEqual(len(self.a.cached), 2)

    def test_media(self):
        """media paths"""
        s = self.a.build_page('media')
        # change media prefix
        self.a.media_prefix = "media"
        s = self.a.build_page('media')
        # print(s)
        self.assertIn('<img src="media/logo.png" alt="">', s)
        self.assertIn("included from include1.yaml", s)
        self.assertIn("from file include/simple/include.html", s)
        self.assertIn("from file include/simple/include.md", s)
        self.assertIn("<li>embedded list 1</li>", s)

    def test_static(self):
        """Write some pages to disk. Please remove '_static_site' if exists
        before to run"""
        self.a.write_static('home', destination='../_static_site',
                            media_prefix='media',
                            static_url='/my_path/')
        self.assertTrue(os.path.exists('../_static_site/home.html'))
        self.a.write_static(['home', 'media', 'page1/page1', 'embedding'],
                            destination='../_static_site',
                            media_prefix='media',
                            static_url='/my_path/')
        self.assertTrue(os.path.exists('../_static_site/page1/page1.html'))
        self.a.write_static(destination='../_static_site',
                            media_prefix='media',
                            static_url='/my_path/')
        self.assertTrue(os.path.exists('../_static_site/blog/blog1.html'))
        self.a.write_json_meta(destination='../_static_site')
        self.assertTrue(os.path.exists('../_static_site/site_meta.json'))

    def test_meta(self):
        """read metadata from pages"""

        m = self.a.meta_pages('blog')
        self.assertIn("blog/blog1", m)
        self.assertIn("home", self.a.vars['meta_pages'])

        m = self.a.vars['meta_pages']['media']
        self.assertEqual(m["author"], "Almoststatic")
        m = self.a.vars['meta_pages']['home']
        self.assertEqual(m["author"], "Claudio Driussi")

    def test_blog(self):
        """blog info in metadata"""

        # blog info into pages
        m = self.a.vars['meta_pages']
        self.assertIn("blog", m['blog/blog1'])
        self.assertNotIn("blog", m["page1"])

        # all pages with or without empty_ok
        pages, cat, tag = self.a.query_blog()
        self.assertEqual(len(pages), 0)
        pages, cat, tag = self.a.query_blog(empty_ok=True)
        self.assertEqual(len(pages), 4)

        # list of id's
        ids = self.a.pages_id(cat['python'])
        self.assertEqual(len(ids), 2)

        # non existing keys
        pages, cat, tag = self.a.query_blog(categories=['no_categ'])
        self.assertEqual(len(cat), 0)
        pages, cat, tag = self.a.query_blog(tags=['no_tag'], empty_ok=True)
        self.assertEqual(len(pages), 4)
        self.assertEqual(len(tag), 0)

        # a single category
        pages, cat, tag = self.a.query_blog(categories=['python'])
        self.assertEqual(len(cat), 1)


if __name__ == '__main__':
    unittest.main()
