# What is?

**Almoststatic** is a static sites and web pages generator engine written in
[Python](https://python.org) which uses the
[Jinja2 template system](https://jinja.palletsprojects.com) to render pages.
It can be integrated with [Flask](https://flask.palletsprojects.com) apps to
serve static contents on dynamic sites or can be used stand alone for static
sites development.

Pages are declared in [yaml](https://yaml.org/) files and rendered with Jinja2
template files, the html contents can be written in [markdown](https://www.markdownguide.org/)
markup language or in plain html. "content" folder contains all data needed to
do the job, the "config.yaml" is used to share global parameters and to setup
configuration.

It is roughly inspired by [hugo](https://gohugo.io/) static site generator,
but it differs in many ways.

# Why Almoststatic?

There are many static site generators such as [Next.js](https://nextjs.org/),
[Hugo](https://gohugo.io/) or [Jekyll](https://jekyllrb.com/), but you can
prefer Almostatic because:

- **It's perfect for pythonist.** It uses Jinja2 and Flask which are widely
  used by python community, so you don't need to learn other programming
  languages or template engines.
- **It's easy!** In fact the rules are very few and this mean few things to
  learn and great flexibility.
- **It's versatile.** It's engine has a powerful recursive system for embedding
  and including contents. This helps you to build rich contents and also to
  split them in small pieces called *"widgets"* easier to maintain.
- **It has blog capabilities.** Contain functions used to query metadata info
  useful to organize blog pages by categories and tags.
- **You can deliver static and dynamic contents at same time.** With Flask you
  can build your dynamic content and let Almoststatic to render the rest of
  page or the whole page, if it is full static.
- **Write static sites.** Static sites are composed only by text files and media
  contents. They are easy to deliver on the web, are secure by design, require
  less maintenance and resources and are faster. If you have no need for dynamic
  contents, with **Almoststatic** you can write all pages as static.
- **Not only for pythonists.** Basic knowledge of python is needed, but once
  your developer environment is ready, Almoststatic lets you to focus on
  writing yaml and markdown's contents and to create your own widgets in html
  and Jinja2.

# About this site

**This site is not designed to be nice, but to illustrate Almoststatic capabilities.**

I'm not a web designer so forgive me if the site looks ugly. My purpose was to build
a site written in plain [Bootstrap](https://getbootstrap.com/) css engine with
samples of all functionality of **Almoststatic**.

Please dig into source code and discover some best practices. As I previous said,
the rules are few so is very important to adopt a clean and consistent working
method. But this is not a rule! If you find a better work organization you are
free to adopt it.

Keep in mind that this site has only demo purpose, if you want to reuse widgets
or derive custom themes, please see the
[Flatstep theme](https://gitlab.com/almoststatic-themes/flatstep)

# Status of project

Almoststatic is young but stable! It has all planned features and always gave me
the right results so it can be considered "production ready"

At the moment there are only a few themes, we are developing a "rolling theme"
with some beautiful widgets ready to use.
See [Flatstep theme](https://gitlab.com/almoststatic-themes/flatstep)
and follow the tutorial.

I'm not a designer so the result is of average quality. But I'm sure that good
designers can write great themes.

# What's next?

If you like what you see and what I said, you can read the full documentation
and build your own themes. And if you wish, publish them as open source,
I will be happy to share your links.

# Enjoy!

