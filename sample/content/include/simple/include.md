---
## embedded markdown

from file include/simple/include.md

Some markdown content:

### *italic and bold*

Veniam deserunt **nostrud velit** irure *officia reprehenderit* ex.

### *numbered list*

1. Ex reprehenderit tempor eiusmod deserunt
2. consectetur deserunt eiusmod.

### *table*

Name | url
- | -
Flask | [https://flask.palletsprojects.com]()
Python | [https://www.python.org/]()
Pypi | [https://pypi.org/]()
Read the Docs | [https://readthedocs.org/]()

### *dotted list*

- apples
- bananas
- peaches

### *link*

$flask
