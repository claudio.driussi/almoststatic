---
title: Embedding widgets
description: How to embed widgets within other widgets
author: Claudio Driussi

hero:
  id: hero
  image: hero/hero1.jpg

particles:
  - type: text_html
    id: particle1
    text: |
      ## Particle
      This is a particle

content:
  - type: text_html
    id: intro
    class: info-box
    text: |
      ## Embedding
      To build rich contents, often you need to embed widgets within other
      widgets. With Almoststatic, this is easy.

      Almost all widgets have a `text` parameter where text in html or markdown
      can be written. To embed widgets you have only to add a `content` list
      to the `text` parameter, this mean that within a widget you can store
      a list of as many widgets you need.

      But there is more. In embedded widgets you can embed other contents in
      recursive way. Please look at the source code to see how it works.

  - type: tabbed
    style: "
      background-color: bisque;
      "
    id: my_tab
    tabs:
      - label: tab1
        active: True
        text: |
          ## Simple content without embedding

          Et pariatur eiusmod enim sit irure id sunt fugiat ipsum tempor pariatur
          esse consectetur anim. Cupidatat irure ullamco exercitation fugiat id
          voluptate irure ut in officia eu reprehenderit et consectetur.
          Do culpa aliqua mollit adipisicing dolore cupidatat amet laboris incididunt
          nostrud magna dolor elit. Laboris excepteur dolore consequat ipsum sunt
          velit aute culpa laborum non dolore fugiat veniam. Sit voluptate tempor
          velit ex do consequat in duis fugiat velit.

          Excepteur quis deserunt occaecat deserunt et laboris Lorem reprehenderit
          aliquip. Deserunt velit deserunt magna laborum eu laboris aute irure
          occaecat minim sunt cupidatat non ipsum. Occaecat amet qui eu nulla
          occaecat ullamco id esse. Ea qui cupidatat in reprehenderit aliqua velit
          Lorem aliquip eu in. Reprehenderit in elit minim do elit cillum do non.
          Quis reprehenderit nostrud deserunt deserunt.

      - label: tab2
        text:
          content:
            - type: carousel
              id: carousel1
              btn_class: "btn btn-primary btn-lg"
              slides:
                - image: [images/landscape1.jpg, "image_class", "image_alt"]
                  align: text-start
                  text: |
                    # My first image

                    This is a markdown *embedded* into a yaml file
                    <b>bold text with inline html</b>

                    Second row with an empty line between.
                  buttons: [["Learn more", "#"]]

                - image: [images/landscape5.jpg, "image_class1", "image_alt1"]
                  text:
                    content:
                      - type: image_row
                        id: imgrow2
                        left: 2
                        title: It's nested!
                        image: "images/face8.jpg"
                        text: |
                          Mollit dolore **qui minim aliquip** eu. Ipsum ea sint id occaecat irure quis ullamco do veniam exercitation consectetur quis.

                  buttons:
                    [
                      ["Gallery", "#", "btn btn-info"],
                      ["Once more", "#", "btn btn-primary"],
                    ]

                - image: [images/landscape4.jpg, "image_class2", "image_alt2"]
                  align: text-end
                  text: |
                    # lorem text
                    Exercitation nisi officia sint elit. Laboris excepteur do officia est aliquip voluptate esse mollit culpa velit exercitation. Amet incididunt elit cillum dolor quis cillum. Voluptate id ad consequat duis proident fugiat elit dolor quis reprehenderit id. Sunt et aliqua elit irure nisi id irure sint irure aliquip id eu. Cupidatat duis sit consectetur voluptate. Velit amet quis proident esse sit.
                  buttons: [["Learn more", "#", "btn btn-info"]]

      - label: tab3
        text:
          content:
            - type: text_html
              id: embedded_test
              text: |
                ## tab 3 with multiple widgets
                embedded text

            - type: accordion
              id: fisa1
              cards:
                - button: "Card 1"
                  show: True
                  text: |
                    Aute sunt Lorem aute sunt esse et enim dolore ad voluptate nisi occaecat.
                    Sint consectetur eu aliquip ex elit. Tempor sunt ea culpa in laboris culpa laborum culpa.

                - button: "Card 2"
                  text: |
                    Consequat mollit laboris voluptate ex laboris esse nostrud esse.
                    Incididunt labore aliquip excepteur consequat. Nisi enim aliqua id reprehenderit
                    exercitation dolore labore ea ea.

                - button: "Card 3"
                  text: |
                    ## Title
                    Tempor cupidatat laborum consectetur eiusmod cupidatat exercitation reprehenderit
                    Lorem adipisicing cillum esse id nisi laborum. Id tempor amet esse sint ex minim.voluptate.

            - type: image_row
              id: imgrow2
              left: 2
              title: It's nested!
              image: "images/face8.jpg"
              text: |
                Mollit dolore **qui minim aliquip** eu. Ipsum ea sint id occaecat irure quis ullamco do veniam exercitation consectetur quis.

      - label: tab4
        text:
          content:
            - type: text_html
              id: nested
              text: |
                ## tab 3
                nested content

            - type: tabbed
              id: my_nested_tab
              tabs:
                - label: nested_tab1
                  active: True
                  text: |
                    ## nested tab 1
                    simple content

                - label: nested_tab2
                  text:
                    content:
                      - type: text_html
                        id: nested inclusion
                        text: |
                          ## nested tab 2 with recursive content
                          recursive content

  - type: text_html
    class: info-box
    id: embed
    text: |
      ## Embedding within text

      Almoststatic have another powerful way to embed widgets. With the
      command `embed` (which is a `context_processor` in terms of flask)
      and the ability of *Jinja2* to assign data at runtime, you can store data
      and render it with a widget.

      With:

          {{'{% set myvar = [
            {
              "type": "list_",
              "id": "my_list",
              "items_": ["my_list element 1", "my_list element 2", "my_list element 3"]
            }
          ]
          %}'}}

      you store the data and then with `{{'{{ embed(myvar) }}'}}` you call the widget to render it

      The following is an example:

  - type: text_html
    id: embed_html
    text: |
      <h2>My embedded content</h2>
      {% set myvar = [
        {
          "type": "list_",
          "id": "my_list",
          "items_": ["my_list element 1", "my_list element 2", "my_list element 3"]
        }
      ]
      %}
      {{ embed(myvar) }}

  - type: text_html
    class: info-box
    id: particle
    text: |
      ## Embedding with particles

      Another useful Almoststatic way to embed widgets is using particles.
      With the command `particle` (which is a `context_processor` in terms of
      flask) you can render a widget decladerd in a special section called
      `particles` by default.

      This is handy because let you to store embedding data into a separate
      section in a cleaner way.

      The following is an example:

  - type: text_html
    id: embed_html
    text: |
      ## My text with particle embedded

      Aute sunt Lorem aute sunt esse et enim dolore ad voluptate nisi occaecat.
      Sint consectetur eu aliquip ex elit. Tempor sunt ea culpa in laboris culpa laborum culpa.

      ---
      {{particle('particle1')}}

      ---
      Consequat mollit laboris voluptate ex laboris esse nostrud esse.
      Incididunt labore aliquip excepteur consequat. Nisi enim aliqua id reprehenderit
      exercitation dolore labore ea ea.
