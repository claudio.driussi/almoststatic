"""Static contents with Almoststatic are best served with "get_media" command,
but if you need to use flask standard method with "url_for" command, you can
write a special flask app with static_url_path configuration
"""
from flask import Flask
from flask import make_response

from almoststatic import FlaskAlmoststatic

fas = FlaskAlmoststatic()
fas.load_config()

STATIC_URL = '/my_path/'
MEDIA_PREFIX = 'media'
DESTINATION = '../_static_site'

app = Flask(__name__, static_url_path=STATIC_URL+MEDIA_PREFIX)
fas.init_app(app)


@app.route('/')
def index():
    fas.write_static(destination=DESTINATION,
                     media_prefix=MEDIA_PREFIX,
                     static_url=STATIC_URL)
    fas.write_json_meta(destination=DESTINATION)
    return make_response("Done!")


if __name__ == '__main__':
    app.run()
