import time
import datetime
import logging
from io import StringIO

from flask import Flask
from flask import render_template, make_response, request

from ftpsync.targets import FsTarget
from ftpsync.ftp_target import FTPTarget
from ftpsync.synchronizers import UploadSynchronizer
from ftpsync.util import set_pyftpsync_logger

from almoststatic import FlaskAlmoststatic

app = Flask(__name__)
app.config['SECRET_KEY'] = 'my_secret_flask_key'
app.jinja_env.auto_reload = True
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['FAS_CONFIG'] = "./content/config.yaml"

fas = FlaskAlmoststatic()
fas.init_app(app)
fas.md_extensions.append('abbr')


@app.route('/')
def index():
    return fas_pages("index")


@app.route('/<path:page>', methods=['GET', 'POST'])
def fas_pages(page):
    if page == 'write_static':
        return write_static()
    if not fas.cache:
        fas.load_config()
    page_text = fas.build_page(page)
    if not page_text:
        return page_not_found(404)
    return make_response(page_text)


@app.route('/dynamic')
def dynamic():
    ts = time.time()
    dd = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
    tt = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
    page_text = fas.build_page('dynamic',
                               dynamic='This is a dynamic content!',
                               t_date=dd,
                               t_time=tt)
    if not page_text:
        return page_not_found(404)
    return make_response(page_text)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('page404.html', page={}), 404

# ----------------------------------------------------------------------------
# write static pages and send them with ftp
# ----------------------------------------------------------------------------


# configure writing pages variables
MEDIA_PREFIX = 'media'
STATIC_URL = '/'
DESTINATION = '../_static_site'
OUT_PAGES = ['write_static', 'write_done', 'dynamic']

# configure ftp parameters
FTP_URL = "** Please configure FTP on flaskapp.py before sending files **"
FTP_USER = "your_user"
FTP_PASSWD = "your_secret_password"
FTP_OPTS = {"no_color": True}

# write log to a string to send it to html page
stream = StringIO()
ftplog = logging.getLogger("write_stream")
ftplog.addHandler(logging.StreamHandler(stream))
set_pyftpsync_logger(ftplog)


def write_static():
    """write static pages"""
    if request.method == 'POST':
        # write html pages to local filesystem
        fas.write_static(destination=DESTINATION,
                         media_prefix=MEDIA_PREFIX,
                         static_url=request.form['form1-static_url'],
                         out_pages=OUT_PAGES)
        # no more required, metadata is always saved on media folder
        # fas.write_json_meta(destination=DESTINATION)

        # send pages to ftp site
        if request.form.get('form1-static-ftp_site', "off") == 'on':
            local = FsTarget(DESTINATION)
            remote = FTPTarget(
                "/", FTP_URL, username=FTP_USER, password=FTP_PASSWD)
            s = UploadSynchronizer(local, remote, FTP_OPTS)
            s.run()

        # send media files to ftp site if required
        if request.form.get('form1-static-ftp_media', "off" == 'on'):
            local = FsTarget("content/media")
            remote = FTPTarget(f"/{MEDIA_PREFIX}", FTP_URL,
                               username=FTP_USER, password=FTP_PASSWD)
            s = UploadSynchronizer(local, remote, FTP_OPTS)
            s.run()

        # get log string and render the result page
        log = stream.getvalue()
        log = log.replace('\n', '<br>').replace('[0K', '')
        stream.truncate(0)
        page_text = fas.build_page('write_done', ftplog=log)
        return make_response(page_text)

    # page with form to request writing static pages
    page_text = fas.build_page(
        'write_static',
        destination=DESTINATION,
        media=MEDIA_PREFIX,
        ftp_url=FTP_URL)
    return make_response(page_text)


if __name__ == '__main__':
    app.run()
